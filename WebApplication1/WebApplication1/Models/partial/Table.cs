﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [MetadataType(typeof(TablesMetadata))]
    public partial class Table
    {
    }

    public class TablesMetadata
    {
        [Required(ErrorMessage ="必填")]
        public int Id { get; set; }

        [Required(ErrorMessage = "必填")]
        public string Name { get; set; }

        [Required(ErrorMessage = "必填")]
        public string Email { get; set; }

        [Required(ErrorMessage = "必填")]
        public string password { get; set; }

        public bool Gender { get; set; }
        public System.DateTime Birthday { get; set; }
    }
}