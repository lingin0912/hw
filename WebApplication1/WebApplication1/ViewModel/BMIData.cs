﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModel
{
    public class BMIData
    {

        [Required(ErrorMessage ="必填")]
        [Range(30,300,ErrorMessage ="30~300")]
        public float Weight { get; set; }

        [Required(ErrorMessage = "必填")]
        [Range(50, 200, ErrorMessage = "30~300")]
        public float Height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }
    }
}